

params.transcriptomeindex = "/project/wherrylab/wlabtools/Ref_files/hg38/hg38_kallistoindex/hu_indx.idx"
params.name          = "RNA-Seq Abundance Analysis"
params.reads         = "$baseDir/rawfiles/*.fastq"
params.fragment_len  = '180'
params.fragment_sd   = '20'
params.bootstrap     = '10'
params.output        = "Kallistoresults/"


log.info "K A L L I S T O - N F  ~  version 0.9"
log.info "====================================="
log.info "name                   : ${params.name}"
log.info "reads                  : ${params.reads}"
log.info "transcriptomeindex     : ${params.transcriptomeindex}"
log.info "fragment length        : ${params.fragment_len} nt"
log.info "fragment SD            : ${params.fragment_sd} nt"
log.info "bootstraps             : ${params.bootstrap}"
log.info "output                 : ${params.output}"
log.info "\n"


/*
 * Input parameters validation
 */

transcriptomeindex     = file(params.transcriptomeindex)

/*
 * validate input files
 */
if( !transcriptomeindex.exists() ) exit 1, "Missing transcriptome file: ${transcriptomeindex}"

/*
 * Create a channel for read files 
 */
 
Channel
    .fromFilePairs( params.reads, size: -1 )
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    .set { read_files } 



process mapping {
    tag "reads: $name"
    cpus 4
    publishDir "${params.output}", mode: 'move'
    input:
    file index from transcriptomeindex
    set val(name), file(reads) from read_files

    output:
    file "kallisto_${name}" into kallisto_out_dirs 

    script:
    //
    // Kallisto tools mapper
    //
    def single = reads instanceof Path
    if( !single ) {
        """
        mkdir kallisto_${name}
        kallisto quant -b ${params.bootstrap} -i ${index} -t ${task.cpus} -o kallisto_${name} ${reads}
        """
    }  
    else {
        """
        mkdir kallisto_${name}
        kallisto quant --single -l ${params.fragment_len} -s ${params.fragment_sd} -b ${params.bootstrap} -i ${index} -t ${task.cpus} -o kallisto_${name} ${reads}
        """
    }

}


